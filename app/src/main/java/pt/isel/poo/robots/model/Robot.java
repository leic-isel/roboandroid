package pt.isel.poo.robots.model;

public class Robot extends Actor {
    static int counter=0;
    //public int id;
    Robot(Position position, Game game) {
        super(position, game);
        /*id = */counter++;
        //game.changedActor(this);
    }
    Robot() {}
    @Override
    void move() {
        Position target = game.getHero().position;
        Dir dir = Dir.fromTo(position, target);
        move(dir);
        //System.out.print("R"+id+' ');
    }
    @Override
    boolean isMovable() { return true; }
    @Override
    void collideTo(Actor a) {
        if (a instanceof Robot) {
            a.destroy();
            this.destroy();
            new Junk(a.position,game);
        }
        else
            a.collideFrom(this);
    }
    @Override
    void destroy() { --counter; super.destroy(); }
}
