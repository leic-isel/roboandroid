package pt.isel.poo.robots.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Person extends Actor {
    private boolean dead = false;
    Person(int l, int c, Game g) {
        super(l,c,g);
    }
    Person() {}
    @Override
    void move() { }
    @Override
    void move(Dir dir) {
        if (dir == Dir.JUMP)
            move(game.arena.randomFreePosition());
        else
            super.move(dir);
    }
    @Override
    boolean isMovable() { return true; }
    @Override
    void collideTo(Actor a) {
        destroy();
    }
    @Override
    void collideFrom(Actor a) {
        game.changedActor(a);
        destroy();
    }
    @Override
    void destroy() {
        dead = true;
        game.changedActor(this);
    }
    public boolean isDead() {
        return dead;
    }

    @Override
    public void save(DataOutputStream dos) throws IOException {
        super.save(dos);
        dos.writeBoolean(dead);
    }

    @Override
    public void load(DataInputStream dis) throws IOException {
        super.load(dis);
        dead = dis.readBoolean();
    }
}
