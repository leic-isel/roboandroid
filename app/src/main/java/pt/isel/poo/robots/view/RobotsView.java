package pt.isel.poo.robots.view;

import pt.isel.poo.robots.model.*;

import pt.isel.poo.tile.Tile;
import pt.isel.poo.tile.TilePanel;

public class RobotsView implements GameListener {
    private Game model;
    private TilePanel panel;
    private ActorView[][] tmpPanel;

    public RobotsView(Game model, TilePanel panel) {
        this.model = model;
        this.panel = panel;
        model.addListener(this);
        int lines = model.getLines();
        int cols = model.getCols();
        panel.setSize(cols,lines);
        tmpPanel = new ActorView[lines][cols];
    }

    private ActorView getTile(Position p) {
        ActorView t = tmpPanel[p.line][p.col];
        if (t==null) {
            t = (ActorView) panel.getTile(p.col, p.line);
            panel.setTile(p.col, p.line, null);
        } else
            tmpPanel[p.line][p.col] = null;
        return t;
    }

    private void setTile(Tile t, Position p) {
        Tile o = panel.getTile(p.col,p.line);
        if (o != null)
            tmpPanel[p.line][p.col] = (ActorView) o;
        panel.setTile(p.col,p.line,t);
    }

    @Override
    public void actorMoved(Actor actor, Position old) {
        Tile t = getTile(old);
        setTile(t, actor.getPosition());
    }

    @Override
    public void actorRemoved(Position pos) {
        getTile(pos);
    }

    @Override
    public void actorCreated(Actor actor) {
        Position pos = actor.getPosition();
        Tile t = null;
        if (actor instanceof Person) t = new PersonView((Person)actor);
        if (actor instanceof Robot) t = new RobotView();
        if (actor instanceof Junk) t = new JunkView();
        assert t!=null;
        setTile(t,pos);
    }

    @Override
    public void actorChanged(Actor actor) {
        Position p = actor.getPosition();
        panel.invalidate(p.col,p.line);
    }

    @Override
    public void gameOver(boolean winner) { }

    public void reset() {
        for (int l = 0; l < model.getLines(); l++)
            for (int c = 0; c < model.getCols(); c++) {
                panel.setTile(c, l, null);
                tmpPanel[l][c] = null;
            }
    }
}
