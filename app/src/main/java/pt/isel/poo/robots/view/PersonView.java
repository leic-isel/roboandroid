package pt.isel.poo.robots.view;

import android.graphics.Canvas;

import pt.isel.poo.robots.R;
import pt.isel.poo.robots.model.Person;
import pt.isel.poo.tile.Img;

public class PersonView extends ActorView {
    private Person person;
    PersonView(Person p) { person = p; }

    private static Img img= new Img(ctx, R.drawable.man);
    private static Img dead= new Img(ctx, R.drawable.dead);

    @Override
    public void draw(Canvas canvas, int side) {
        Img i = person.isDead() ? dead : img;
        i.draw(canvas,side,side,p);
    }
}
