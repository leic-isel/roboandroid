package pt.isel.poo.robots.view;

import android.graphics.Canvas;
import pt.isel.poo.robots.R;
import pt.isel.poo.tile.Img;

public class JunkView extends ActorView {
    private static Img img= new Img(ctx, R.drawable.junk);

    @Override
    public void draw(Canvas canvas, int side) {
        img.draw(canvas,side,side,p);
    }
}
