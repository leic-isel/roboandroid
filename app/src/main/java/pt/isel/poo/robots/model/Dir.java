package pt.isel.poo.robots.model;

public enum Dir {
    UP(-1,0),DOWN(+1,0),LEFT(0,-1),RIGHT(0,+1),
    UP_LEFT(-1,-1),UP_RIGHT(-1,+1),DOWN_LEFT(+1,-1),DOWN_RIGHT(+1,+1),
    NONE(0,0), JUMP(0,0);

    public final int dl;
    public final int dc;

    Dir(int dl, int dc) {
        this.dl = dl;
        this.dc = dc;
    }

    public static Dir fromTo(Position from, Position to) {
        int dl = normalize(to.line - from.line);
        int dc = normalize(to.col - from.col);
        for (Dir dir : values())
            if (dir.dl==dl && dir.dc==dc) return dir;
        assert false;
        return null;
    }

    private static int normalize(int delta) {
        return delta>=1 ? 1 : (delta<=-1 ? -1 : 0);
    }
}
