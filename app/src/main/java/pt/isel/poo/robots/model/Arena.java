package pt.isel.poo.robots.model;

import java.util.Iterator;
import java.util.LinkedList;

public class Arena implements Iterable<Actor> {
    private Actor[][] map;

    Arena(int lines, int cols) {
        map = new Actor[lines][cols];
    }

    void set(Actor actor) {
        map[actor.position.line][actor.position.col] = actor;
    }
    Actor get(Position pos) {
        return map[pos.line][pos.col];
    }
    void reset(Position pos) { map[pos.line][pos.col]=null; }


    @Override
    public Iterator<Actor> iterator() {
        return new Iterator<Actor>() {
            private Position cur = getFirst();
            @Override
            public boolean hasNext() { return cur!=null; }
            @Override
            public Actor next() {
                Actor a = get(cur); cur=getNext(cur); return a;
            }
        };
    }

    private Position getNext(Position cur) {
        int line = cur.line;
        int col = cur.col+1;
        if (col>=map[0].length) { line++; col=0; }
        for (int l=line; l<map.length ; ++l)
            for(int c=(l==line)?col:0; c<map[0].length ; ++c) {
                Actor a = map[l][c];
                if (a!=null) return a.position;
            }
        return null;
    }

    private Position getFirst() {
        for (Actor[] actors : map)
            for (Actor a : actors)
                if (a!=null) return a.position;
        return null;
    }

    Position randomFreePosition() {
        Position pos;
        do pos = Position.random();
        while (get(pos)!=null);
        return pos;
    }

    LinkedList<Actor> removeMovables() {
        LinkedList<Actor> lst = new LinkedList<>();
        for (Actor actor : this) {
            if (actor.isMovable()) {
                lst.add(actor);
                reset(actor.position);
            }
        }
        return lst;
    }

    void clear() {
        for (Actor actor : this)
            reset(actor.position);
    }
}
