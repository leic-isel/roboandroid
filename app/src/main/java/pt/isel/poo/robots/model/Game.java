package pt.isel.poo.robots.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

public class Game implements Iterable<Actor> {
    public static final int MAX_ROBOTS = 10;
    Person hero;
    Arena arena;

    public Game(int lines, int cols) {
        Position.init(lines,cols);
        arena = new Arena(lines,cols);
    }

    public void startLevel(int leveNumber) {
        arena.clear();
        hero = new Person(getLines()/2,getCols()/2,this);
        Robot.counter = 0;
        for (int i = 0; i < MAX_ROBOTS * leveNumber; i++)
            new Robot(arena.randomFreePosition(),this);
    }

    public int getLines() { return Position.getLines(); }
    public int getCols() { return Position.getCols();  }

    public void moveHero(Dir dir) {
        if (hero.isDead()) return;
        LinkedList<Actor> movables = arena.removeMovables();
        hero.move(dir);
        for (Actor actor : movables)
            actor.move();
        //System.out.println();
    }

    public Person getHero() { return hero; }

    @Override
    public Iterator<Actor> iterator() { return arena.iterator(); }

    void movedActor(Position old, Actor actor) {
        //arena.reset(old);
        Actor a = arena.get(actor.position);
        if (a!=null) {
            actor.position = old;
            actor.collideTo(a);
        } else {
            arena.set(actor);
            for (GameListener listener : listeners)
                listener.actorMoved(actor,old);
        }
    }

    public boolean isFinish() {
        return hero.isDead() || Robot.counter==0;
    }

    private LinkedList<GameListener> listeners = new LinkedList<>();

    public void addListener(GameListener listener) {
        listeners.add(listener);
    }

    void createActor(Actor actor) {
        arena.set(actor);
        for (GameListener listener : listeners)
            listener.actorCreated(actor);
    }

    void destroyedActor(Actor actor) {
        Actor other = arena.get(actor.position);
        if (other==actor)
            arena.reset(actor.position);
        for (GameListener listener : listeners)
            listener.actorRemoved(actor.position);
    }

    void changedActor(Actor actor) {
        for (GameListener listener : listeners)
            listener.actorChanged(actor);
    }

    public void save(DataOutputStream dos) throws IOException {
        for(Actor a : arena) {
            dos.writeUTF(a.getClass().getSimpleName());
            a.save(dos);
        }
        dos.writeUTF("END");
    }
    public void load(DataInputStream dis) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        for(;;) {
            String name = dis.readUTF();
            if (name.equals("END")) break;
            Class cls = Class.forName("pt.isel.poo.robots.model."+name);
            Actor a = (Actor) cls.newInstance();
            a.game = this;
            a.load(dis);
            createActor(a);
            if (a instanceof Person)
                hero = (Person) a;
        }
    }
}
