package pt.isel.poo.robots.model;

public class Junk extends Actor {
    Junk(Position position, Game game) {
        super(position, game);
    }
    Junk() {}
    @Override
    void move() { }
    @Override
    boolean isMovable() { return false; }
    @Override
    void collideFrom(Actor a) { a.destroy();  }
}
