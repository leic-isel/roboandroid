package pt.isel.poo.robots;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import pt.isel.poo.robots.model.Dir;
import pt.isel.poo.robots.model.Game;
import pt.isel.poo.robots.model.Position;
import pt.isel.poo.robots.view.ActorView;
import pt.isel.poo.robots.view.RobotsView;
import pt.isel.poo.tile.OnTileTouchListener;
import pt.isel.poo.tile.TilePanel;

public class RobotsActivity extends Activity {
    private static final int LINES = 10, COLS = 10;
    Game model;
    RobotsView view;

    int levelNumber = 1;
    boolean end=false;

    TilePanel panel;
    Button jump, newGame;
    TextView msg;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.robots);
        ActorView.setContext(this);
        panel = findViewById(R.id.panel);
        jump = findViewById(R.id.jump);
        newGame = findViewById(R.id.newGame);
        msg = findViewById(R.id.msg);

        model = new Game(LINES,COLS);
        view = new RobotsView(model,panel);
        if (state==null)
            model.startLevel(levelNumber++);

        jump.setOnClickListener(v -> onClickJump());
        newGame.setOnClickListener( v-> onClickNewGame() );
        newGame.setEnabled(false);
        msg.setText("");

        panel.setListener(new OnTileTouchListener() {
            @Override
            public boolean onClick(int x, int y) {
                if (end) return false;
                Position to = Position.of(y,x);
                Dir dir = Dir.fromTo(model.getHero().getPosition(),to);
                model.moveHero(dir);
                endConditions();
                return true;
            }

            @Override
            public boolean onDrag(int xFrom, int yFrom, int xTo, int yTo) { return false; }
            @Override
            public void onDragEnd(int x, int y) { }
            @Override
            public void onDragCancel() { }
        });
    }

    private void onClickNewGame() {
        view.reset();
        if (model.getHero().isDead())
            levelNumber=1;
        model.startLevel(levelNumber++);
        jump.setEnabled(true);
        newGame.setEnabled(false);
        msg.setText("");
        end = false;
    }

    private void onClickJump() {
        model.moveHero(Dir.JUMP);
        endConditions();
    }

    private void endConditions() {
        if (model.isFinish()) {
            msg.setText(model.getHero().isDead()
                    ?getString(R.string.loseMsg):getString(R.string.winMsg));
            newGame.setEnabled(true);
            jump.setEnabled(false);
            end =true;
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle state) {
        super.onSaveInstanceState(state);
        ByteArrayOutputStream baos;
        try (DataOutputStream dos =
                     new DataOutputStream(baos=
                             new ByteArrayOutputStream())) {
            state.putInt("LEVEL",levelNumber);
            model.save(dos);
        } catch (IOException e) {
            e.printStackTrace(); return;
        }
        state.putByteArray("MODEL",baos.toByteArray());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle state) {
        super.onRestoreInstanceState(state);
        try(DataInputStream dis =
                    new DataInputStream(
                            new ByteArrayInputStream(state.getByteArray("MODEL")))) {
            model.load(dis);
            levelNumber = state.getInt("LEVEL");
            endConditions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
