package pt.isel.poo.robots.view;


import android.content.Context;
import android.graphics.Paint;

import pt.isel.poo.tile.Tile;

public abstract class ActorView implements Tile {
    protected static Context ctx;
    public static void setContext(Context c) { ctx = c; }
    protected static Paint p = new Paint();

    @Override
    public boolean setSelect(boolean selected) {
        return false;
    }
}
