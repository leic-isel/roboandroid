package pt.isel.poo.robots.model;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Actor {
    Position position;
    Game game;

    Actor(int line, int col, Game game) {
        this(Position.of(line,col),game);
    }

    Actor(Position pos, Game game) {
        position = pos;
        this.game = game;
        game.createActor(this);
    }
    Actor() {}

    void move(Dir dir) {
        move(position.add(dir));
    }
    void move(Position p) {
        Position old = position;
        position = p;
        game.movedActor(old,this);
    }
    public Position getPosition() { return position; }

    abstract void move();
    abstract boolean isMovable();

    void collideTo(Actor a) { assert false; }
    void collideFrom(Actor a) { assert false; }
    void destroy() { game.destroyedActor(this);  }

    public void save(DataOutputStream dos) throws IOException {
        dos.writeByte(position.line);
        dos.writeByte(position.col);
    }

    public void load(DataInputStream dis) throws IOException {
        position = Position.of(dis.readByte(),dis.readByte());
    }
}
